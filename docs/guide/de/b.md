[![Finally a fast CMS](https://www.finally-a-fast.com/logos/logo-cms-readme.jpg)](https://www.finally-a-fast.com/) Finally a fast CMS - Helpers
================================================

Introduction
------------

* [About FAFCMS](a.md)


Getting Started
---------------

* [What do you need to know](b.md)
