[![Finally a fast CMS](https://www.finally-a-fast.com/logos/logo-cms-readme.jpg)](https://www.finally-a-fast.com/) Finally a fast CMS - Helpers
================================================

[![Latest Stable Version](https://poser.pugx.org/finally-a-fast/fafcms-helpers/version)](https://packagist.org/packages/finally-a-fast/fafcms-helpers)
[![Total Downloads](https://poser.pugx.org/finally-a-fast/fafcms-helpers/downloads)](https://packagist.org/packages/finally-a-fast/fafcms-helpers)
[![Yii2](https://img.shields.io/badge/Powered_by-Yii_Framework-green.svg?style=flat)](http://www.yiiframework.com/)
[![License](https://poser.pugx.org/finally-a-fast/fafcms-helpers/license)](https://packagist.org/packages/finally-a-fast/fafcms-helpers)


This package provides basic helpers for the Finally a fast CMS.

Installation
------------

The preferred way to install this extension is through [composer](https://getcomposer.org/download/).

Either run
```
php composer.phar require finally-a-fast/fafcms-helpers
```
or add
```
"finally-a-fast/fafcms-helpers": "dev-master"
```
to the require section of your `composer.json` file.

Documentation
------------

[Documentation](https://www.finally-a-fast.com/packages/fafcms-helpers/docs) can found at https://www.finally-a-fast.com/packages/fafcms-helpers/docs.

You can also [download](https://www.finally-a-fast.com/packages/fafcms-helpers/docs/download) the documentation at https://www.finally-a-fast.com/packages/fafcms-helpers/docs/download.

License
-------

**fafcms-helpers** is released under the MIT License. See the [LICENSE](LICENSE) for details.
